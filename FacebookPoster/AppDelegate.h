//
//  AppDelegate.h
//  FacebookPoster
//
//  Created by Vitaliy Zhukov on 06.07.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

